# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here


CSS:每個button 都有自己的位置,在undo redo 有特別設定成fixed,會一直在螢幕的左下方
html:設定button 的型態,如果onclicked 的話會呼叫js裡面的function .另外有用img 來將button 換成載好的image 
     另外有設置三個選單,分別是Brush Size color Font_size.利用選單的方式讓整個畫面比較簡潔
javascript:
	press_reset:按下即將畫布清空
	press_eraser:比較特別的是不是弄成白色筆刷,而是利用globalCompositeOperation = 'destination-out'的方式將畫布上面的圖形抹去.
        press_brush:純粹畫出線條,但可以利用ctx.strokeStyle = $('#selColor').val() , ctx.lineWidth = $('#selWidth').val()來改變顏色和畫筆寬度
	press_text: 按一下button 再按一下畫面,打字會出現在按下去的地方,也可以利用ctx.font = $('#selSize').val() 來調整字的大小
	press_circle:類似前面的press_brush,但是畫出固定大小的空心circle.
	press_rectangle:類似前面的press_brush,但是畫出固定大小的空心rectangle.
	press_triangle:類似前面的press_brush,但是畫出固定大小的空心triangle.
	press_triangle_img:可以直接用滑鼠拉出隨意大小的triangle.
	press_circle_img:可以直接用滑鼠拉出隨意大小的circle.
	press_rectangle_img:可以直接用滑鼠拉出隨意大小的rectangle.
	press_undo:利用儲存在陣列的方式去將之前每一筆動作記住,當按下此按鍵時會將除了上一筆的其他動作重新draw一次
	press_redo:利用儲存在陣列的方式去將之前每一筆動作記住,當按下此按鍵時會將前一筆的動作回復.需要在按過undo後才會有作用
 	cPush:是和redo undo 一起使用的,cPush會放在前面每一個function裡面.
	press_download:利用image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream")把圖片存到img裡面,並給到link的href裡面,最後存檔為my-image.png
	upload:試著用jquery 的方式實作,利用 src = URL.createObjectURL(file)   URL.revokeObjectURL(src) 這兩個來達成將file 上傳上來



